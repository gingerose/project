import React from "react";
import Header from './js/header'
import Main from './js/main'
import Footer from './js/footer'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      search: '',
      category: '',
    }
    
  }

  updateData = (value) => {
    this.setState({ search: value })
  }
  categoryData = (value) => {
    this.setState({ category: value })
  }
  render() {
    console.log(this.state.search)
    return (
      <>
        <Header data={this.props.data}
          search={this.updateData} category = {this.categoryData}></Header>
        <Main data={this.props} search={this.state.search} category={this.state.category}></Main>
        <Footer></Footer>
        <div id="popup"></div>
        <div id="popup__tickets"></div>
      </> 
    );
  }

}
export default App;

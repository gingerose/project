import React from "react";

class Comments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            body: "",
            comments: [],
        }
        this.add_name = this.add_name.bind(this);
        this.add_body = this.add_body.bind(this);
        this.add_comment = this.add_comment.bind(this);
    }
    add_name(value) {
        this.setState({ name: value })
    }
    add_body(value) {
        this.setState({ body: value })
    }
    add_comment() {
        //ReactDOM.render(<Comment comment={this.state}></Comment>, document.querySelector("#comment__field"))
        if (this.state.name !== "" || this.state.body !== "") {
            let comment = {
                name: this.state.name,
                body: this.state.body
            }
            document.getElementById('comm__name').value = "";
            document.getElementById('comm__body').value = "";
            this.setState((state) => {
                state.comments.push(comment)
                return {
                    comments: state.comments,
                    name: "",
                    body: "",
                }
            });
        }
    }
    render() {
        console.log(this.state.comments)
        let new_comms = [];
        for (let i = 0; i < this.state.comments.length; i++) {
            new_comms.push(<>
                <div className="comment">
                    <picture>
                        <img alt="user" src="https://busheyautomotive.com/wp-content/uploads/2016/02/default-profile-pic.png"></img>
                    </picture>
                    <div className="comment__body">
                        <p>{this.state.comments[i].name}</p>
                        <p>{this.state.comments[i].body}</p>
                    </div>
                </div>
            </>)
        }

        return (
            <><h1>Comments</h1>
                <section id="comments">
                    <div className="comments__name">
                        <label htmlFor="name">Your name: </label><br />
                        <input id="comm__name" type="text" name="name" onChange={(e) => { this.add_name(e.target.value) }}></input>
                    </div>
                    <div className="comments__body">
                        <label htmlFor="body">Your comment: </label><br />
                        <textarea id="comm__body" name="body" onChange={(e) => { this.add_body(e.target.value) }}></textarea>
                    </div>
                    <button className="add__btn" onClick={this.add_comment}>Add</button>
                    <div id="comment__field">
                        {
                            new_comms
                        }
                    </div>
                </section>
            </>
        );
    }

}
export default Comments;
import React from "react";
import { Swiper, SwiperSlide } from 'swiper/react'
import SwiperCore, {Navigation, Pagination} from 'swiper'
import 'swiper/swiper-bundle.css';
SwiperCore.use([Navigation, Pagination]);
class Slider extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const slides = [];
    for (let i = 0; i < 5; i += 1) {
      slides.push(
        <SwiperSlide key={`slide-${i}`}>
          <img className="slider__img" src={`${this.props.data[i].src}`}
            alt={`slide img ${i}`} />
        </SwiperSlide>
      )
    }
    return (
      <React.Fragment>
        <Swiper id="slider" tag="section" navigation pagination>
          {slides}
        </Swiper>
      </React.Fragment>
    )
  }

}
export default Slider;

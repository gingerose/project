import React from "react";
class Tickets extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      price: 0,
      counter: 0,
    }
    this.add_ticket = this.add_ticket.bind(this);
    this.hide = this.hide.bind(this);
    this.reset = this.reset.bind(this);
  }
  hide() {
    document.getElementById('tickets__information').style.display = 'none';
    document.getElementById('film__information').style.display = 'block';
    for (let i = 0; i < document.getElementsByClassName('seat').length; i++) {
      document.getElementById(`seatId${i}`).style.backgroundColor = '#b4392ba1';
    }
    this.setState({
      price: 0,
      counter: 0,
    })
  }
  reset() {
    for (let i = 0; i < document.getElementsByClassName('seat').length; i++) {
      document.getElementById(`seatId${i}`).style.backgroundColor = '#b4392ba1';
    }
    this.setState({
      price: 0,
      counter: 0,
    })
  }
  add_ticket(i) {
    if (document.getElementById(`seatId${i}`).style.backgroundColor !== 'rgb(240, 215, 134)') {
      document.getElementById(`seatId${i}`).style.backgroundColor = '#f0d786';
      this.setState({
        price: this.state.price + this.props.film.price,
        counter: this.state.counter + 1,
      })
    }
  }
  buy() {
    for (let i = 0; i < document.getElementsByClassName('seat').length; i++) {
      if (document.getElementById(`seatId${i}`).style.backgroundColor.toString() === 'rgb(240, 215, 134)') {
        document.getElementById(`seatId${i}`).style.backgroundColor = 'rgba(253, 253, 253, 0.658)';
      }
    }
  }
  render() {
    let seats = [];
    for (let i = 0; i < 112; i++) {
      seats.push(
        <>
          <div onClick={(e) => { this.add_ticket(i) }} key={`${i}`} id={`seatId${i}`} className="seat"></div>
        </>
      )

    }
    return (
      <>
        <div id="tickets__information">
          <div onClick={this.hide} aria-label="Close" className="close-popup js-close-campaign"></div>
          <p className="title_tick">By tickets on {this.props.film.title}</p>
          <div className="tickets">
            <div className="your__tickets">
              <img alt="film" src={`${this.props.film.picture}`}></img><br /><br />
            Price for 1: {this.props.film.price}<br />
            Your tickets:<br />
            Amount of your tickets: {this.state.counter}<br />
            Price: {this.state.price}
            </div>

            <div id="hall">
              {seats}
            </div>
            <div className="buy__btn" onClick={this.buy}>Buy tickets</div>
            <div className="buy__btn" onClick={this.reset}>Reset</div>
          </div>

        </div>

      </>
    );
  }

}
export default Tickets;
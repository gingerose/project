import React from "react";
import Menu from './menu'
class Header extends React.Component {
    constructor(props) {
        super(props);
        this.updateData = this.updateData.bind(this);
        this.categoryData = this.categoryData.bind(this);
    }
    updateData = (value) => {
        this.props.search(value)

    }
    categoryData = (value) => {
        this.props.category(value)
    }
    render() {

        return (
            <>
                <header>

                    <div className="block">
                        <hr className="split"></hr>
                        <Menu updateData={this.updateData} category={this.categoryData}></Menu>
                        <hr className="split"></hr>
                    </div>
                </header>
            </>
        );
    }

}
export default Header;

import React from "react";
import ReactDOM from 'react-dom';
class Gallery extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            counter: 5,
            i: 0,
        }
        this.next_page = this.next_page.bind(this);
        this.prev_page = this.prev_page.bind(this);
        
    }
    next_page() {
        this.setState((state) => {
            // if (window.innerWidth <= 1640) {
            //     return {
            //         counter: state.counter + 4,
            //         i: state.i + 4,
            //     }
            // }
            return {
                counter: state.counter + 5,
                i: state.i + 5,
            }
        });
    }
    prev_page() {
        this.setState((state) => {
            // if (window.innerWidth <= 1640) {
            //     return {
            //         counter: state.counter - 4,
            //         i: state.i - 4,
            //     }
            // }
            return {
                counter: state.counter - 5,
                i: state.i - 5,
            }
        });
    }
    hide() {
        document.getElementById('warning_pages').style.display = 'none';
    }
    render() {
        console.log(window.width)
        let pictures = [];
        // if (window.innerWidth <= 1640) {
        //     this.state.counter = 4;
        // }
        // else if(window.innerWidth > 1640){
        //     this.state.counter = 5;
        // }
        for (let i = 0; i < this.props.films.length; i++) {
            pictures.push(
                <picture className="gallery__picture" key={`${this.props.films[i].id}`}>
                    <img alt="film pict" src={`${this.props.films[i].picture}`}></img>
                </picture>
            );

        }

        if (this.state.i < 0) {
            this.next_page()
        }
        if (this.state.counter > this.props.films.length + 5) {
            const element = <p>There is all photos</p>
            ReactDOM.render(element, document.getElementById('warning_pages'));
            document.getElementById('warning_pages').style.display = 'block';
            this.prev_page()
        }
        pictures = pictures.slice(this.state.i, this.state.counter);

        return (
            <>
                <hr className="split"></hr>
                <h1 id="gallery">Gallery</h1>
                <section className="gallery__">
                    {pictures}
                </section>

                <div onClick={this.hide} className="pages">
                    <div onClick={this.prev_page} className="page">&#5130;</div>
                    <div onClick={this.next_page} className="page">&#5125;</div>
                </div>
                <div id="warning_pages">
                </div>
                <hr className="split"></hr>
            </>
        );
    }

}
export default Gallery;
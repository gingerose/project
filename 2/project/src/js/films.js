import React from "react";
import ReactDOM from 'react-dom'
import Information from "./information"
class Filters extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <>
                <div className="filter" id="filters">
                    <span style={{ color: "#c0bab996" }}>Filter by: </span>
                    <label>
                        <input
                            onChange={(event) => {
                                this.props.checke2d(event.target.checked);
                            }}
                            className="checkbox" type="checkbox" name="sort"></input>
                        <span className="fieldSort">{this.props.filters[0]}</span>
                    </label>
                    <span className="splits"> | </span>
                    <label>
                        <input
                            onChange={(event) => {
                                this.props.checke3d(event.target.checked);
                            }}
                            className="checkbox" type="checkbox" name="sort"></input>
                        <span className="fieldSort">{this.props.filters[1]}</span>
                    </label>
                    <span className="splits"> | </span>
                    <label>
                        <input
                            onChange={(event) => {
                                this.props.age_limit(event.target.value);
                            }}
                            className="input" type="input" name="filter"></input>
                        <span className="fieldFilter">{this.props.filters[2]}</span>
                    </label>
                </div>
            </>
        );
    }

}
class Sort extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <>
                <div className="filter" id="filters">
                    <span style={{ color: "#c0bab996" }}>Sort by: </span>
                    <label>
                        <input
                            onChange={(event) => {
                                this.props.rating(event.target.checked);
                            }}
                            className="checkbox" type="checkbox" name="sort"></input>
                        <span className="fieldSort">{this.props.sorts[0]}</span>
                    </label>
                    <span className="splits"> | </span>
                    <label>
                        <input
                            onChange={(event) => {
                                this.props.price(event.target.checked);
                            }}
                            className="checkbox" type="checkbox" name="sort"></input>
                        <span className="fieldSort">{this.props.sorts[1]}</span>
                    </label>
                </div>
            </>
        );
    }

}
class Film extends React.Component {
    constructor(props) {
        super(props);
        this.onclick = this.onclick.bind(this);
    }
    onclick () {
        ReactDOM.render(<Information film={this.props.film}/>, document.querySelector("#popup"));
        document.getElementById('film__information').style.display = 'block';
        document.getElementById('main').style.filter = 'blur(5px)';
      }
    render() {
        let key = 100;
        let sessions = [];
        for (let i = 0; i < this.props.film.sessions.length; i++) {

            sessions.push(
                <div key = {key} className="session">
                    {this.props.film.sessions[i]}
                </div>)
                key +=10;
        }
        return (
            <>
                <div className="film">
                    <picture className="film__img">
                        <img alt="film" className="film__image" src={`${this.props.film.picture}`}></img>
                    </picture>
                    <div className="film__sessions">
                        <h3 onClick={(e) => this.onclick(e)}>{this.props.film.title}</h3>
                        <div className="sessions">
                            {sessions}
                        </div>
                        <div className="price_rating">Price {this.props.film.price}</div>
                        <div className="price_rating">Rating {this.props.film.rating}</div>
                    </div>
                </div>
                {/* <Test film = {this.props.film}></Test> */}
            </>
        );
    }
}
class Films extends React.Component {
    constructor(props) {
        super(props);
        this.refs = React.createRef();
        this.state = {
            counter: 8,
            checke2D: false,
            checke3D: false,
            age_limit: '',
            rating: false,
            price: false,
            all_films: false,

        }
        this.click = this.click.bind(this)
        this.all_films = this.all_films.bind(this)
        this.category = this.category.bind(this)
    }
    hide(){
        document.getElementById('field_warning').style.display = 'none';
    }
    click() {

        this.setState((state) => {
            if (state.counter > this.props.films.length ) {
                document.getElementById('field_warning').style.display = 'block';
                const element = <p>There is all films</p>
                ReactDOM.render(element, document.getElementById('field_warning'));
            }
            else {
                document.getElementById('field_warning').style.display = 'none';
            }
            return {
                counter: state.counter + 8,
            }
        });
    }
    checked_2D = (value) => {
        this.setState({ checke2D: value })

    }
    checked_3D = (value) => {
        this.setState({ checke3D: value })

    }
    age_limit = (value) => {
        this.setState({ age_limit: value })
    }
    rating_sort = (value) => {
        this.setState({ rating: value })
    }
    price_sort = (value) => {
        this.setState({ price: value })
    }
    all_films() {
        this.setState({ all_films: true })
    }
    category() {
        this.setState({ all_films: false })
    }
    render() {
        let films = [];
        let search_films = [];
        let key = -1;
        console.log(window.innerWidth)
        if (this.props.search.toLowerCase() === "" || (this.state.checke3D === true && this.state.checke2D === true)) {
            for (let i = 0; i < this.props.films.length; i++) {
                search_films.push(this.props.films[i]);
            }
        }
        else {
            for (let i = 0; i < this.props.films.length; i++) {
                if (this.props.films[i].title.toLowerCase().includes(this.props.search.toLowerCase())) {

                    search_films.push(this.props.films[i]);
                }
                if (search_films.length === 0) {
                    document.getElementById('field_error').style.display = 'block';
                    const element = <p>There is no films with such titles</p>
                    ReactDOM.render(element, document.getElementById('field_error'));
                }
                else {
                    document.getElementById('field_error').style.display = 'none';
                }
            }
        }

        if (this.props.category.toLowerCase() !== "" && this.state.all_films === false) {
            search_films = search_films.filter(x => x["genre"] === this.props.category)
        }
        if (this.state.checke2D === true && this.state.checke3D === false) {
            search_films = search_films.filter(x => x["releace"] === '2D')
        }
        if (this.state.checke3D === true && this.state.checke2D === false) {
            search_films = search_films.filter(x => x["releace"] === '3D')
        }
        if (this.state.age_limit !== '') {
            search_films = search_films.filter(x => x["age_limit"] == parseInt(this.state.age_limit), 10)
        }
        if (this.state.rating === true) {
            search_films = search_films.sort((a, b) => a['rating'] < b['rating'] ? 1 : -1);
        }
        if (this.state.price === true) {
            search_films = search_films.sort((a, b) => a['price'] > b['price'] ? 1 : -1);
        }
        for (let i = 0; i < search_films.length; i++) {
            films.push(
                <Film key = {`${key}`} film={search_films[i]}>
                </Film>
            );
            key -= 2;

        }
        return (
            <>
            {/* <Router>
            <Route path={`/film/`} exact component={Test}/>
            </Router> */}
                <h1>Released Films</h1>

                <section className="filter_sort">
                    <Filters filters={["2D", "3D", "Age limit"]} checke2d={this.checked_2D} checke3d={this.checked_3D} age_limit={this.age_limit}></Filters>
                    <Sort sorts={["Rating", "Price"]} rating={this.rating_sort} price={this.price_sort}></Sort>
                </section>
                <div className="path">
                    <span>Films &#62; {this.props.category}</span>
                </div>
                <section onClick={this.hide} id="films">
                    {films.slice(0, this.state.counter)}
                </section>
                <button onClick={this.click} className="next__btn">Next</button>
                <div id="field_warning"></div>
            </>
        );
    }
}
export default Films;
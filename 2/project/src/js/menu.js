import React from "react";

class Menu extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
                <section className="menu">
                    <div className="menu__inline">
                    <div className="search">
                            <input className="fieldSearch"
                                id="search__input"
                                type="text"
                                name="search"
                                placeholder="Enter film to search"
                                ref="search__input"
                                onChange={(event) => {
                                    this.props.updateData(event.target.value)
                                }}></input>
                            <button className="search__btn">Search</button>
                            <div id = "field_error"></div>
                        </div>
                        <nav className="navMenu">
                            <ul className="menuList">
                                <li className="menuLi" key="Films"><a 
                                 onClick={() => {
                                    this.props.category("")
                                }}
                                href="#films">Films</a>
                                    <div className="dropdown-content">
                                        <a
                                            onClick={() => {
                                                this.props.category("Drama")
                                            }}
                                            className="submenu" href="#drama">Drama</a>
                                        <a 
                                        onClick={() => {
                                            this.props.category("Thriller")
                                        }}
                                        className="submenu" href="#thriller">Thriller</a>
                                        <a 
                                        onClick={() => {
                                            this.props.category("Comedy")
                                        }}
                                        className="submenu" href="#comedy">Comedy</a>
                                        <a 
                                        onClick={() => {
                                            this.props.category("Adventure")
                                        }}
                                        className="submenu" href="#adventure">Adventure</a>
                                        <a 
                                        onClick={() => {
                                            this.props.category("Horror")
                                        }}
                                        className="submenu" href="#horror">Horror</a>
                                    </div>
                                </li>
                                <span className="splits"> | </span>
                                <li className="menuLi" key="Contacts"><a href="#contacts">Contacts</a>
                                    <div className="dropdown-content">
                                        <a className="submenu" href="#location">Location</a>
                                        <a className="submenu" href="#information">Information</a>
                                    </div>
                                </li>
                                <span className="splits"> | </span>
                                <li className="menuLi" key="Gallery"><a href="#gallery">Gallery</a></li>
                                <span className="splits"> | </span>
                                <li className="menuLi" key="Comments"><a href="#comments">Comments</a></li>
                            </ul>
                        </nav>
                        
                    </div>
                </section>
            </>
        );
    }

}
export default Menu;

import React from "react";
import Films from './films'
import Slider from './slider'
import Gallery from './gallery'
import Contacts from "./contacts"
import Comments from "./comments"
class Main extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    //console.log(this.props.category)
    return (

      <>
        <main id="main">
          <Slider data={this.props.data.data}></Slider>
          <Films films={this.props.data.films} search = {this.props.search} category = {this.props.category}></Films>
          <Gallery films={this.props.data.films}></Gallery>
          <Contacts></Contacts>
          <Comments></Comments>
        </main>
      </>
    );
  }

}
export default Main;

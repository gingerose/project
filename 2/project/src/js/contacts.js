import React from "react";
import Location from './map'

class Contacts extends React.Component {

    render() {
        return (
            <>
                <section id="contacts">
                    <h1>Location</h1>
                    <Location></Location>
                    <h1>Information</h1>
                    <div id="information">
                        <div className="information__block">
                            <p>Phone numbers:</p>
                            <p>&#9990; +380663471243</p>
                            <p>&#9990; +750023412344</p>
                        </div>
                        <div className="information__block">
                            <p>Mails:</p>
                            <p>&#9993; thebestcinema@gmail.com</p>
                            <p> &#9993; cinemaforever@gmail.com</p>
                        </div>
                        <div className="information__block">
                            <p>Sosial network:</p>
                            <p>Instagram @cinemabest</p>
                            <p>Twitter @cinemaforever</p>
                        </div>
                    </div>
                </section>
                <hr className="split"></hr>
            </>
        );
    }

}
export default Contacts;
import React from "react";
import ReactDOM from 'react-dom'
import Tickets from "./tickets"
class Information extends React.Component {
    constructor(props) {
        super(props);
        this.click_tickets = this.click_tickets.bind(this);
    }
    
    click_tickets(){
        ReactDOM.render(<Tickets film={this.props.film}/>, document.querySelector("#popup__tickets"));
        document.getElementById('tickets__information').style.display = 'block';
        document.getElementById('film__information').style.display = 'none';
    }
    hide() {
        document.getElementById('film__information').style.display = 'none';
        document.getElementById('main').style.filter = 'blur(0px)';
    }
    render() {
        let sessions = [];
        for (let i = 0; i < this.props.film.sessions.length; i++) {

            sessions.push(
                <div className="session">
                    {this.props.film.sessions[i]}
                </div>)
        }
        return (
            <>
                <div id="film__information">
                    <div className="information__img">
                        <img alt = "film" className="information__image" src={`${this.props.film.picture}`}></img>
                    </div>

                    <div className="information">
                        <p className="title">{this.props.film.title}</p>

                        <div className="info__all">
                            <p className="info">Price: {this.props.film.price}</p>
                            <p className="info">Rating: {this.props.film.rating}</p>
                            <p className="info">Genre: {this.props.film.genre}</p>
                            <p className="info">Releace: {this.props.film.releace}</p>
                            <p className="info">Duration: {this.props.film.duration}</p>
                            <p className="info">Country: {this.props.film.country}</p>
                        </div>

                    </div>
                    <div onClick={this.hide} aria-label="Close" className="close-popup js-close-campaign"></div>
                    <div className="info__sessions">
                        <div onClick={this.click_tickets} className="sessions">
                            {sessions}
                        </div>
                    </div>
                    <div className="description">{this.props.film.description}</div>
                </div>
            </>
        );
    }

}
export default Information;
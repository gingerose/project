import React from "react";
class Footer extends React.Component {
  render() {
    return (
      <>
        <footer>
          <hr className="split"></hr>
          <p>&#9400; Author: Kurzhumova Mariia</p>
        </footer>
      </>
    );
  }

}
export default Footer;
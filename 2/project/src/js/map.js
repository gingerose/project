import React from 'react'
import { Map, TileLayer, Marker } from 'react-leaflet';

class Location extends React.Component {
    render() {
        return (
            <section id="location">
                <Map
                    center={[50.5, 30.5]}
                    zoom={6}
                    maxZoom={10}
                    attributionControl={true}
                    zoomControl={true}
                    doubleClickZoom={true}
                    scrollWheelZoom={true}
                    dragging={true}
                    animate={true}
                    easeLinearity={0.35}
                >
                    <TileLayer
                        url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
                    />
                    <Marker position={[50.5, 30.5]}>
                    </Marker>
                </Map>
            </section>
        );
    }
}

export default Location
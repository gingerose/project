import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
let films = require("./films_.js")
let sliderData = [
  {
    src: "https://multiplex.ua/images/08/61/08619fb4ec829c556e01abfcde7285bb.jpg",
  },
  {
    src: "https://multiplex.ua/images/fb/0b/fb0b4f2003a75aaa078cc0172629ec4b.jpg",
  },
  {
    src: "https://multiplex.ua/images/15/71/1571bfae0f47de97971c910b8979064e.jpg",
  },
  {
    src: "https://multiplex.ua/images/6d/18/6d183eeaf99078920b6f0b485a51db7c.jpg",
  },
  {
    src: "https://multiplex.ua/images/49/87/4987a6e138fe345aeb3ae4ba031b3b90.jpg",
  },
];
ReactDOM.render(<App data={sliderData} films={films.films} search/>, document.querySelector("#root"));

